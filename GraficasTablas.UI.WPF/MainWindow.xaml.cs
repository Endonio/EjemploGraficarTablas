﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GraficaTablas.COMMON;
using GraficaTablas.BIZ;
using System.Windows.Controls.DataVisualization.Charting;

namespace GraficasTablas.UI.WPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Personas[] _personas;
        public MainWindow()
        {
            InitializeComponent();
            cmbTipoGrafica.Items.Add("Por Edad");
            cmbTipoGrafica.Items.Add("Por Genero");
            cmbTipoGrafica.Items.Add("Por Intervalo");
        }

        private void btnGenerar_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            try
            {
                Generadora g = new Generadora();
                _personas = g.GenerarPersonas(int.Parse(txtCantidad.Text), 18, 30);
                dgDatos.ItemsSource = _personas;
                MessageBox.Show("Datos Generados Correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Generar los Datos: " + ex.Message);
                _personas = null;
            }
            this.Cursor = Cursors.Arrow;
        }

        private void btnGenerarGrafico_Click(object sender, RoutedEventArgs e)
        {
            if (_personas==null)
            {
                MessageBox.Show("No se han generado datos para gráficar...");
                return;
            }
            Grafica.Series.Clear();

            ServicioDeDatosParaGraficacion s = new ServicioDeDatosParaGraficacion(_personas);

            if (cmbTipoGrafica.Text == "Por Edad")
            {
                DatoPorEdad[] datos = s.DatosPorEdad();
                dbTabla.ItemsSource = datos;
                ColumnSeries serie = new ColumnSeries();
                serie.DependentValuePath = "Cantidad";
                serie.IndependentValuePath = "Edad";
                serie.Title = "Cantidad";
                serie.ItemsSource = datos;
                Grafica.Series.Add(serie);
            }
            else if (cmbTipoGrafica.Text == "Por Genero")
            {
                DatoPorGenero[] datos = s.DatosParaHombresMujeres();
                dbTabla.ItemsSource = datos;
                BarSeries serie = new BarSeries();
                serie.DependentValuePath = "Cantidad";
                serie.IndependentValuePath = "Sexo";
                serie.Title = "Cantidad";
                serie.ItemsSource = datos;
                Grafica.Series.Add(serie);
            }
            else
            {
                Intervalo[] intervalos = new Intervalo[3];
                Intervalo i = new Intervalo();
                i.LimiteInferior = 1.5f;
                i.LimiteSuperior = 1.7f;
                intervalos[0] = i;
                i = new Intervalo();
                i.LimiteInferior = 1.7f;
                i.LimiteSuperior = 1.9f;
                intervalos[1] = i;
                i = new Intervalo();
                i.LimiteInferior = 1.9f;
                i.LimiteSuperior = 2.1f;
                intervalos[2] = i;
                DatoIntervalo[] datos = s.EstaturaCantidadGenero2(intervalos);
                dbTabla.ItemsSource = datos;

                ColumnSeries serieHombres = new ColumnSeries();
                serieHombres.IndependentValuePath = "Intervalo";
                serieHombres.DependentValuePath = "Hombres";
                serieHombres.Title = "Hombres";
                serieHombres.ItemsSource = datos;

                ColumnSeries serieMujeres = new ColumnSeries();
                serieMujeres.IndependentValuePath = "Intervalo";
                serieMujeres.DependentValuePath = "Mujeres";
                serieMujeres.Title = "Mujeres";
                serieMujeres.ItemsSource = datos;

                Grafica.Series.Add(serieHombres);
                Grafica.Series.Add(serieMujeres);
            }
        }
    }
}
