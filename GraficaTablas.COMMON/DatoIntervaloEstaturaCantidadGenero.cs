﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraficaTablas.COMMON
{
    public class DatoIntervaloEstaturaCantidadGenero
    {
        public Intervalo IntervaloDeEstatura { get; set; }
        public string Sexo { get; set; }
        public int Cantidad { get; set; }
    }
}
