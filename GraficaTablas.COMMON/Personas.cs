﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraficaTablas.COMMON
{
    public class Personas
    {
        public int Edad { get; set; }
        public float Estatura { get; set; }
        public string Sexo { get; set; }

    }
}
