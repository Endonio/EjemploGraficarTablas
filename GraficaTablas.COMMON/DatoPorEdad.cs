﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraficaTablas.COMMON
{
    public class DatoPorEdad
    {
        public int Edad { get; set; }
        public int Cantidad { get; set; }
    }
}
