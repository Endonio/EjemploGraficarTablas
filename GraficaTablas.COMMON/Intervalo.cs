﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraficaTablas.COMMON
{
    public class Intervalo
    {
        public float LimiteInferior { get; set; }
        public float LimiteSuperior { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", LimiteInferior, LimiteSuperior);
        }
    }
}
