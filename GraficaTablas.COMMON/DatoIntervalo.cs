﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraficaTablas.COMMON
{
    public class DatoIntervalo
    {
        public Intervalo Intervalo { get; set; }
        public int Hombre { get; set; }
        public int Mujeres { get; set; }
    }
}
