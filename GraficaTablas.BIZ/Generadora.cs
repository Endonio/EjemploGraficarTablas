﻿using GraficaTablas.COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraficaTablas.BIZ
{
    public class Generadora
    {
        public Personas[] GenerarPersonas (int numero, int edadMinima,int edadMaxima)
        {
            Random r = new Random();
            Personas[] personas = new Personas[numero + 1];
            for (int i = 0; i <= numero; i++)
            {
                Personas p = new Personas();
                p.Edad = r.Next(edadMinima, edadMaxima);
                p.Estatura = (float)(r.NextDouble() * (2.1-1.5)+1.5);
                p.Sexo = r.Next(0, 2)==0?"Masculino":"Femenino";
                personas[i] = p;
            }
            return personas;
        }
    }
}
