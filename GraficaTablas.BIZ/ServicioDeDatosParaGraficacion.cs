﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraficaTablas.COMMON;

namespace GraficaTablas.BIZ
{
    public class ServicioDeDatosParaGraficacion
    {
        Personas[] _personas;
        public ServicioDeDatosParaGraficacion(Personas[] personas)
        {
            _personas = personas;
        }

        public DatoPorGenero[] DatosParaHombresMujeres()
        {
            DatoPorGenero[] datos = new DatoPorGenero[2];
            DatoPorGenero hombres = new DatoPorGenero();
            hombres.Sexo = "Masculino";
            hombres.Cantidad = _personas.Count(p => p.Sexo == "Masculino");
            DatoPorGenero mujeres = new DatoPorGenero();
            mujeres.Sexo = "Femenino";
            int cantidad = 0;
            for (int i = 0; i < _personas.Length; i++)
            {
                if (_personas[i].Sexo=="Femenino")
                {
                    cantidad++;
                }
            }
            mujeres.Cantidad = cantidad;
            datos[0] = hombres;
            datos[1] = mujeres;
            return datos;
        }

        public DatoPorEdad[] DatosPorEdad()
        {
            int edadMinima = BuscarEdadMinima();
            int edadMaxima = BuscarPorEdadMaxima();
            DatoPorEdad[] datos = new DatoPorEdad[edadMaxima - edadMinima + 1];
            //30-18=12{18,19,20,21,22,23,24,25,26,27,28,29,30}
            for (int i = 0; i < datos.Length; i++)
            {
                DatoPorEdad dato = new DatoPorEdad();
                dato.Edad=edadMinima+i;
                dato.Cantidad = _personas.Count(p => p.Edad == dato.Edad);
                datos[i] = dato;
            }
            return datos;
        }

        private int BuscarPorEdadMaxima()
        {
            //Con expresiones Lambda
            return _personas.Max(p => p.Edad);
        }

        private int BuscarEdadMinima()
        {
            //Con ciclos
            int centinela = int.MaxValue;
            for (int i = 0; i < _personas.Length; i++)
            {
                if (_personas[i].Edad<centinela)
                {
                    centinela = _personas[i].Edad;
                }
            }
            return centinela;
        }

        public DatoIntervaloEstaturaCantidadGenero[] EstaturaCantidadGenero(Intervalo[] intervalos)
        {
            DatoIntervaloEstaturaCantidadGenero[] datos = new DatoIntervaloEstaturaCantidadGenero[intervalos.Length*2];
            int j = 0;
            for (int i = 0; i < intervalos.Length*2; i=i+2)
            {
                //Mujeres
                DatoIntervaloEstaturaCantidadGenero datoM = new DatoIntervaloEstaturaCantidadGenero();
                datoM.IntervaloDeEstatura = intervalos[j];
                datoM.Sexo = "Femenino";
                datoM.Cantidad = _personas.Count(p => p.Sexo == "Femenino" && p.Estatura >= intervalos[j].LimiteInferior && p.Estatura < intervalos[j].LimiteSuperior);
                datos[i] = datoM;

                //Hombres
                DatoIntervaloEstaturaCantidadGenero datosH = new DatoIntervaloEstaturaCantidadGenero();
                datosH.IntervaloDeEstatura = intervalos[j];
                datosH.Sexo = "Masculino";
                datosH.Cantidad = _personas.Count(p => p.Sexo == "Masculino" && p.Estatura >= intervalos[j].LimiteInferior && p.Estatura < intervalos[j].LimiteSuperior);
                datos[i + 1] = datosH;
                j++;
            }
            return datos;
        }

        public DatoIntervalo[] EstaturaCantidadGenero2(Intervalo[] intervalos)
        {
            DatoIntervalo[] datos = new DatoIntervalo[intervalos.Length];
            for (int i = 0; i < intervalos.Length; i++)
            {
                DatoIntervalo d = new DatoIntervalo();
                d.Intervalo = intervalos[i];
                d.Hombre = _personas.Count(p => p.Sexo == "Masculino" && p.Estatura >= intervalos[i].LimiteInferior && p.Estatura < intervalos[i].LimiteSuperior);
                d.Mujeres = _personas.Count(p => p.Sexo == "Femenino" && p.Estatura >= intervalos[i].LimiteInferior && p.Estatura < intervalos[i].LimiteSuperior);
                datos[i] = d;
            }
            return datos;
        }
    }
}
